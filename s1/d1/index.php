<!-- PHP code can be included in another file by using the require once keyword -->

<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S01</title>
</head>
<body style={background-color: coral;}>
	<h1> VARIABLES: </h1>
	<!-- Variables can be used to output data in the double quotes while single quotes will not work -->
	<?php $name = "Jino";?>
	<!-- When echoing constants, omit the $sign -->
	<p><?php echo PI?></p> 
	<p><?php echo $sum?></p> 
	<p><?php echo $address?></p> 
	<p><?php echo $address2?></p> 
	<p><?php echo "Good day $name! Your email is $email."; ?></p>
	<h1> DATA TYPES: </h1>
<!-- Normal echoing fo booleand and null variables will not make them visible to user. Too see their types instead, we can use the following -->
	<p><?php echo gettype($spouse);?></p> 

<!-- To see more detailed info of the boolean, use var_dump -->
	<p><?php echo var_dump($spouse);?></p> 

	<p><?php echo var_dump($hasTravelledAbroad);?></p> 

<!-- var_dump can also be used for arrays -->
	<p><?php echo "---------------- PRINTING ARRAY require [index] or {index}"; ?>
	<p><?php echo ": --- ARRAY MUST BE TO STRING. CANNOT BE ECHOED"; ?>
	<p><?php echo $grades?></p>

	<p><?php echo var_dump($grades);?></p> 
	<p><?php print_r($grades[0]);?></p> 
	<p><?php echo var_dump($grades2);?></p> 
	<p><?php echo print_r($grades2);?></p> 

<!-- Printing Objects -->
	<p><?php echo "---------------- PRINTING OBJECTS require ->"; ?>
	<p><?php echo $gradesObj->firstGrading;?></p> 
	<p><?php echo $personObj->address->state;?></p> 
		<p><?php echo ": --- Use var_dump or print_r to print whole object"; ?>
	<p><?php var_dump($gradesObj)?></p> 
	<p><?php print_r($gradesObj)?></p>

	<h1> OPERATORS: </h1>
	<h3>Arithmetic Operators</h3>
	<p>Sum : <?php echo $x + $y;?></p>
	<p>Difference : <?php echo $x - $y;?></p>
	<p>Product : <?php echo $x * $y;?></p>
	<p>Quotient : <?php echo $x / $y;?></p>

	<h3>Equality Operators</h3>
	<p>Loose Equality: <?php echo var_dump($x == 234); ?></p>
	<p>Loose InEquality: <?php echo var_dump($x != 123); ?></p>

	<p>Strict InEquality: <?php echo var_dump($x === 123); ?></p>
	<p>Strict InEquality: <?php echo var_dump($x !== 234); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser (or equal): <?php echo var_dump($x <= $y); ?></p>
	<p>Is Greater (or equal): <?php echo var_dump($x >= 234); ?></p>

	<h3>Logical Operators</h3>
	<p> Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?>
	<p> Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?>
	<p> Are Some Requirements Met: <?php var_dump(!$isLegalAge && !$isRegistered); ?>

	<h1>Functions: </h1>
	<h4></h4>
	<p>Full name: <?php echo getFullname("John", "J", "Smith")?>
	<p><?php echo determineTyphoonIntensity(12);?></p>
	<p><?php echo isUnderAge(12);?></p>
	<p><?php echo determineUser(4);?></p>

</body>
</html>
