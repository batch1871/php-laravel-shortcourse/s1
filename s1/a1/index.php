<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1 & 2</title>
	<style>
		div.solid {border-style: solid; padding: 20px}
	</style>
</head>
<body>
	<h1>Activity 1</h1>

			    
			    <div class="solid">
			    	<h2>Full Address</h2>
					<p><?php echo getFullAddress("Phillipines", "Quezon City", "Metro Manila", "3F Caswym Bldg., Timog Avenue");?></p>
					<?php echo getFullAddress("Phillipines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue");?>
			    </div>
					
	<h1>Activity 2</h1>

			    <div class="solid">
			    	<h2>Letter-Based Grading</h2>
					<p><?php echo getLetterGrade(87);?></p>
					<p><?php echo getLetterGrade(94);?></p>
					<p><?php echo getLetterGrade(74);?></p>
			    </div>


</body>
</html>